NuneCo is an advisory implementation of the Marathon Manufacturing Methodology.

# Goal
To create secure, accessible, distributed communities.

---
To facilite personal automation, local organization, and efficiency of logistics through research and engineering practices.

===
To accomplish these ends through the pursuit, integration, and application of intersectional perspectives, intentionally holistic and restorative practices, and appropriately transparent non-hierarchical systems.

# Method
Take what is offered, use it as a stepping stone.

Make the way gentler on those who come after.

Contemplate privilege, systemic failure and success, learn from those who came before, and use our collective knowledge to ease the burden of life's waxing and waning upon the vulnerable.

# Boundary Statement
We respect consent.

We expect you to do likewise.

If you will not respect our consent, respect our capability for retribution, and our restraint in reaction to direct threat.

## 0. (active) consent  
We require active consent for participation.

Healthy communication begins with consent, and the necessary boundary of our individual "no" is systemically echo'd by us all.

Accept the retraction of "yes" to mean "no", and do so with grace.

Violation of principle zero is grounds for disassociation and exclusion, and communication of intent and boundary goes far towards avoiding mistakes.

Restraint in humor around this subject is encouraged.

# Guiding Principles
## 1. (process) document  
Record how you do everything, systemically.

This is a large task, at first, but the falloff of repetitive tasks returns valuable data about ongoing and emerging responsibilities.


## 2. (responsibly) disclose  
It is our responsibility to occasionally disclose something private for ethical purposes.

Know the threat level before sharing with other teams or the public, secrets are only maintainable between one and themselves.

A critical vulnerability to one is a vulnerability for all, and remediation must be expedited through direct communication.

## 3. (structurally) facilitate
Prioritize the path to a fix before burning down existing systems.

We all lift together, and structure will aid our efforts.

# Legal

NuneCo (dba [Irregular Team](https://irregular.team/) is a cooperative mutual aid organization functioning to advise and assist organizations in need of security, accessibility, or infrastructure assistance.

For concerns, contact a local representative, or reach out via our internet site's contact page.
