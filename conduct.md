One Rule:  
Consent in all things.  

Three Suggestions:  
Practice self-awareness.  
Respect boundaries.  
Communicate.


# Consent
With proper observation of healthy consent paradigms, both in interpersonal relationships and throughout all interactions among the cooperative friendlies, most other input is intended as a guideline.

Once consent is properly understood, suggestions around self-awareness, guidelines for communication, and respect for boundaries is built into your worldview, as they are prerequisites in consent-based interactions.

Getting there, especially if consent is an alien concept to you, may be a bit of a rough road.

## Self-Awareness
What is the impact of your actions?

What is the impact of your lack of action?

What is your intention, and how did that differ from observational result?

## Boundaries
Ask if you think you might be crossing a boundary.

It is the responsibility of the boundary-creator to communicate the boundary, as undisclosed boundaries are often mistakenly or accidentally traversed.

Once a meeting of the minds, mutual understanding of the boundary, is achieved, we as the boundary-observer are responsible for ongoing respect and communication around the boundary.

## Communication
...and a holistic perspective on how to become "good" at it in a society, are very much outside the scope of this document.

If you aren't already, therapy can help you with communication, as can reading books on the topic or asking your peers for constructive critiques of your past communication habits.