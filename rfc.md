"We"

the collective, cooperative, consenting individuals of 

# NuneCo
...do propose a structure and request for comment

History, and particularly the currently-present covid-19 pandemic, leaves much evidence that humans, particularly during crisis, seek externally organized & imposed structure.

While some structure is or can be healthy and good, too much structure is just as toxic as too much chaos.

Shared definitions must precede sharing of vision, and vision informs priorities.

Undisclosed boundaries will only accidentally be crossed, and disclosed boundaries will be respected.

`tl;dr - balance between chaos & order is good`

## Mutual Aid
...ing one another in communities where intersecting need, past & present trauma, childhood programming, access limitations, triggers both known and unknown, educational privilege, health, age, exposure, or ability may create social friction can be like walking through a multidimensional minefield.

Whatever our healthy limit, as we have ability which matches present or future need, we must lift together, friend.

## Data Preservation
This organization preserves data, with intention of collecting shared knowledge, and of process, research, vision, reason, and making available educational reproducables of functioning local means of community support.

Anvil, forge, welder, sewing machine, lathe, cement mixer, computer.

Kinesthetic representations of our means of production.

## Consent
This org upholds standards of consent and mutual communication.

Seek an active "yes" where equal power exists, and if you are unsure, ask directly.

## Logistics
1=1  
2=3  
3-4=+2

Thirty get forty portions, dividied by threes?

## Problem Solving
Problems will not conform to your expectations.

Be prepared to actively seek the often overlapping symptoms of problem & opportunity to their root cause.



# Legal
See [wiki footer](wiki/_Footer.md).