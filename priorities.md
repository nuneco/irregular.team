To ultimately create cost-neutral, sustainable, automated manufacturing processes that produces accessibile, user-maintainable housing.

Seeking that goal via mobile house (RV, camper, motor home, trailer, etc) rebuilding, and use our initial three months on site to bring online our basic metalworking infrastructure.

The initial three-month plan also includes bringing agriculture, animal care, and programs for transitional & longer term resident use of cooperative resources.

A cooperative trust, for group management of land, tools, infrastructure, vehicles, and other essential shared resources, is the next functional step for the organization.