Most digital infrastructure can be taken offline when active changes aren't happening.

# BeacHead

_@todo - A script to create a pop-up microserver cluster is necessary, see `fsh`._

([vultr pricing](https://www.vultr.com/products/cloud-compute/#pricing) _retrieved 20200616_)

An ipv6-only server from vultr costs $2.50/mo ($0.004/hr).
An ipv4-enabled server from vultr costs $3.50/mo ($0.005/hr).

Both options come with 10gb of drive space, 1/2 terrabyte of network transfer, and are capped at 512MB of RAM and a single CPU.  
Not unlike the Pi Zero W in real-world performance, which is perfect.


A stable cluster (two backend, one frontend) of servers therefore costs $8.50/mo, or $102 per year.

If that cluster is able to take the backend servers offline, or scale up with more servers as necessary, the basic autoscaling entrypoint is ready, and that cost goes down whenever loads are low, which will be normally during the first year.
